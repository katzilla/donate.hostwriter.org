import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#00005a',
    secondary: '#fe5621',
    accent: '#fe5621',
    error: '#c3003e',
    info: '#98a2b9',
    success: '#16ee80',
    warning: '#ebc600',
  },
  iconfont: 'mdi',
  options: {
    customProperties: true,
  }
})
